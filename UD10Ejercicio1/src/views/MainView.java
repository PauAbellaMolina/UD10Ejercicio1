package views;

import javax.swing.JOptionPane;

public class MainView {
	//Metodo que pide un numero al usuario
	public static int pedirNumeroUsuario() throws NumberFormatException {
		String numeroInput = JOptionPane.showInputDialog(null, "Introduce un numero:");
		return Integer.parseInt(numeroInput);
	}
	
	//Metodo que nos muestra si el numero introducido es mayor, menor o igual al aleatorio
	public static void mostrarResultado(String resultado, int numeroInput) {
		System.out.println("El numero introducido (" + numeroInput + ")" + " es " + resultado + " al numero generado aleatoriamente");
	}
	
	//Metodo que finaliza el programa mostrando el resultado final con los intentos echos
	public static void finalizarPrograma(int intentos, int numeroAleatorio) {
		System.out.println("Has adivinado el numero aleatorio (" + numeroAleatorio +") tras " + intentos + " intentos");
	}
}
