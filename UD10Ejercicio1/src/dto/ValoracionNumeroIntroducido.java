package dto;

public class ValoracionNumeroIntroducido {
	//Metodo que devuelve si el valor introducido por el usuario es mayor, menor o igual al generado aleatorimente
	public static String valoracionNumero(int numeroAleatorio, int numeroInput) {
		if (numeroInput > numeroAleatorio) {
			return "mayor";
		} else if (numeroInput < numeroAleatorio) {
			return "menor";
		} else {
			return "igual";
		}
	}
}
