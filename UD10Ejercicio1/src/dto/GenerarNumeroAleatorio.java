package dto;

public class GenerarNumeroAleatorio {
	//Metodo que devuelve un numero aleatorio entre el minimo y maximo pasados
	public static int generarNumeroAleatorio(int min, int max) {
		return min + (int)(Math.random() * ((max - min) + 1));
	}
}
