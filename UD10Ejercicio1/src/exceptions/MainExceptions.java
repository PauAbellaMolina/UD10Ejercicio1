package exceptions;

import views.MainView;
import dto.ValoracionNumeroIntroducido;

public class MainExceptions {
	public static void mainExceptions(int numeroAleatorio) {
		int numeroInput = 0;
		int intentos = 0;
		//Bucle que se ejecuta hasta que el usuario acierte el numero y va sumando el numero de intentos
		do {
			//Probamos a pedir el numero al usuario y recogemos la excepcion NumberFormatException en caso de que el usuario no introduzca un numero
			try {
				numeroInput = MainView.pedirNumeroUsuario();
				//Llamamos al metodo que nos muestra si el numero introducido es mayor, menor o igual al aleatorio
				MainView.mostrarResultado(ValoracionNumeroIntroducido.valoracionNumero(numeroAleatorio, numeroInput), numeroInput);
			}catch(NumberFormatException e) {
				System.out.println("No has introducido un numero");	
			}
			intentos++;
		} while(numeroInput != numeroAleatorio);
		
		//Llamamos al metodo que finaliza el programa mostrando el resultado final con los intentos echos
		MainView.finalizarPrograma(intentos, numeroAleatorio);
	}
}
