import dto.GenerarNumeroAleatorio;
import exceptions.MainExceptions;

public class AdivinarNumApp {
	public static void main(String[] args) {
		//Generamos un numero aleatorio entre 1 y 500
		int numeroAleatorio = GenerarNumeroAleatorio.generarNumeroAleatorio(1, 500);
		
		//Llamamos al metodo principal de la classe de excepciones y le pasamos el numero aleatorio generado
		MainExceptions.mainExceptions(numeroAleatorio);
	}
}
